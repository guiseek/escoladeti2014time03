define(['angular', 'services'], function(angular, services) {
	'use strict';

  /* Directives */
	/*
			$scope.isActive = function (viewLocation) {
				return viewLocation === $location.path();
			};
	*/
	angular.module('myApp.directives', ['myApp.services'])
		.directive('appVersion', ['version', function(version) {
			return function(scope, elm, attrs) {
				elm.text(version);
			};
		}]);
});
