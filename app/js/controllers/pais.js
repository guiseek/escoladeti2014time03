define([], function() {
	return ['$scope', '$http', function($scope, $http) {
		// You can access the scope of the controller from here
		$scope.pageHeader = 'País';
		$scope.panelHeading = 'Lista';

		$scope.heads = ['Código do IBGE','Nome do país','Sigla'];

    $http({method: 'GET', url: '/app/db/paises.json'}).
      success(function(data, status, headers, config) {
        $scope.paises = data;
      }).
      error(function(data, status, headers, config) {
        $scope.debug = data;
      });
      /*
      $scope.update = function(id) {
        console.log(id);

        $http({method: 'PUT', url: 'url'}).
          success(function(data, status, headers, config) {
            $scope.paises = data;
          }).
          error(function(data, status, headers, config) {
            $scope.debug = data;
          });
      };
      */

		// because this has happened asynchroneusly we've missed
		// Angular's initial call to $apply after the controller has been loaded
		// hence we need to explicityly call it at the end of our Controller constructor
		$scope.$apply();
	}];
});