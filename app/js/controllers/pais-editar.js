define([], function() {
	return ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {
		// You can access the scope of the controller from here
		$scope.pageHeader = 'País';
		$scope.panelHeading = 'Editar';
    $scope.panelInfo = 'Uma dica de edição';

    var id = $routeParams.id;
    $scope.pais = {};
    $http({method: 'GET', url: '/app/db/paises.json'}).
      success(function(data, status, headers, config) {
        for (var i = 0; i < data.length; i++) {
          if (id == data[i].id) {
            $scope.pais = data[i];
          };
        };
      }).
      error(function(data, status, headers, config) {
        $scope.debug = data;
      });

		// because this has happened asynchroneusly we've missed
		// Angular's initial call to $apply after the controller has been loaded
		// hence we need to explicityly call it at the end of our Controller constructor
		$scope.$apply();
	}];
});