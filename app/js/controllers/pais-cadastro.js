define([], function() {
	return ['$scope', '$http', function($scope, $http) {
		// You can access the scope of the controller from here
		$scope.pageHeader = 'País';
		$scope.panelHeading = 'Cadastro';
    $scope.panelInfo = 'Uma dica de cadastro';

		// Form
		$scope.master = {};

		$scope.update = function(pais) {
			$scope.master = angular.copy(pais);
		};

    $scope.reset = function() {
      $scope.pais = angular.copy($scope.master);
    };

    $scope.isUnchanged = function(pais) {
      return angular.equals(pais, $scope.master);
    };

    $scope.reset();

		// because this has happened asynchroneusly we've missed
		// Angular's initial call to $apply after the controller has been loaded
		// hence we need to explicityly call it at the end of our Controller constructor
		$scope.$apply();
	}];
});