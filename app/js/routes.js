define(['angular', 'app'], function(angular, app) {
	'use strict';

	return app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
		$routeProvider.when('/view1', {
			templateUrl: 'app/partials/partial1.html',
			controller: 'MyCtrl1'
		});
		$routeProvider.when('/pais', {
			templateUrl: 'app/partials/pais/index.html',
			controller: 'Pais'
		});
		$routeProvider.when('/pais/cadastro', {
			templateUrl: 'app/partials/pais/form-pais.html',
			controller: 'PaisCadastro'
		});
		$routeProvider.when('/pais/editar/:id', {
			templateUrl: 'app/partials/pais/form-pais.html',
			controller: 'PaisEditar'
		});
		$routeProvider.otherwise({redirectTo: '/view1'});

		$locationProvider.html5Mode(true);
	}]);

});